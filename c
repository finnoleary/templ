#!/bin/bash
([ -z "$1" ] || [ "$1" != "init" ] ) && echo "c init [lib]<name>" && exit 1

projname="$2"
[ -d $projname ] && echo $projname "already exists" && exit 1

pattern="lib"
[[ $projname = $pattern* ]] && library=1 || library=0

[ $library == 1 ] && do_command="
  \$SCANBUILD \$CC \$CFLAGS -c *.c -o $projname.o && ar rcs $projname.a *.o
" || do_command="
  \$SCANBUILD \$CC \$CFLAGS *.c -o $projname"

[ $library == 1 ] && clean_command="*.o *.a" || clean_command="$projname"

mkdir $projname
cat > $projname/mkfile << EOF
CC=gcc
CFLAGS= -D_XOPEN_SOURCE=700 -D_FORTIFY_SOURCE=2 -std=c99 -Wall -pedantic -Os -fstack-protector-strong -lrt
CPPCHKFLAGS= --std=posix --std=c99 --platform=unix64 --enable=all
SPARSEFLAGS= -Wsparse-all
SCANBUILDFLAGS= -enable-checker core -enable-checker security -enable-checker unix
SCANBUILD=scan-build $SCANBUILDFLAGS

do:V:$do_command

check:V:
  sparse \$SPARSEFLAGS $projname.c
  cppcheck \$CPPCHKFLAGS $projname.c

test:V:
  \$SCANBUILD \$CC \$CFLAGS test_*.c -o test_$projname

clean:V:
  rm $clean_command
EOF

cat > $projname/$projname.c << EOF
#include "$projname.h"

int main(int argc, char **argv)
{
	return 0;
}
EOF

cat > $projname/$projname.h << EOF
#include <assert.h>
#include <stdlib.h>
#include <strings.h>

EOF

git init $projname
